## GRIPS scraper - for personal use

This is a file scraper for personal use for the elearning grips system.
You can use this to download the resources material from the courses you are registered to on GRIPS.
Download does not include assigment summitions by students).
Source code is avilable.
### Download it from [HERE](https://bitbucket.org/small_hacks/grips_scraper/downloads/MS.jar)

---
**This is a modification** of an existing tool in order to allow competability with the GRIPS system.

The original code can be found [HERE](https://bitbucket.org/pyrexed/ms/src/master/).

The modified code can be found [HERE](https://bitbucket.org/small_hacks/grips_scraper/src/master/).


---
**Requirements**: **Java 8 and above.**

Tested only on windows 64-bit and ubuntu 15.10/amd64.

---

**Usage**:

Double click the JAR file, enter your username and password and press start.

In the Course selection view, select the courses you want to download. You can select the root path where all folders will be created either by editing the text manually or using the right mouse button on the text-field to launch a file/directory chooser.



