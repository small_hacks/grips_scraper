/** Created by roy on 1/23/16.
 *  * modified by elinor to work for UR grips system on 4/7/18.
 */

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.*;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

class MoodleScraper {
    private String LOGIN_URL = "https://elearning.uni-regensburg.de/login/index.php";
    private String MAIN_PAGE = "https://elearning.uni-regensburg.de/";
    private HttpsClient session;
    private String username;
    private String pw;

    private HashMap<String, Course> courses;

    MoodleScraper() {
    }

    Set<String> getCoursesNumbers(){
        return courses.keySet();
    }

    public MoodleScraper(String loginUrl, String mainPage) {
        LOGIN_URL = loginUrl;
        MAIN_PAGE = mainPage;
    }

    private static void print(String msg, Object... args) {
        ControlsPropagator.setLogData(String.format(msg, args));
    }

    void recreateSession() throws Exception {
        createSession(username, pw);
    }

    void createSession(String username, String pw) throws Exception {
        HttpsClient ses = new HttpsClient();
        // make sure cookies is turn on
        CookieHandler.setDefault(new CookieManager());

        // 1. Send a "GET" request, so that you can extract the form's data.
        String page = ses.GetPageContent(LOGIN_URL);
        String postParams = ses.getFormParams(page, username, pw);

        // 2. Construct above post's content and then send a POST request for
        // authentication
        ses.sendPost(LOGIN_URL, postParams);
        session = ses;
        this.username = username;
        this.pw = pw;
    }

    Integer getCourses() throws Exception {
        String result = session.GetContent(MAIN_PAGE);
        HashMap<String, Course> mapp = new HashMap<>();
        Document doc = Jsoup.parse(result, MAIN_PAGE);
        Elements coursesResources = doc.getElementsByClass("qa-course");
        if (coursesResources.isEmpty()) {
            courses = mapp;
            return courses.size();
        }
        for (Element src : coursesResources) {
            Elements links = src.select("a[href]");
            for (Element link : links) {
                String link_address = link.attr("href");

                try {
                    Pattern courseNumRegex = Pattern.compile("\\d{5,12}");
                    Matcher regexMatcher = courseNumRegex.matcher(link_address);
                    while (regexMatcher.find()) {
                        try {
                            URL temp_url = new URL(link_address);
                            //String name = link.text(); // replaced
                            String name= link.attr("title");
                            mapp.put(regexMatcher.group(), new Course(temp_url, URLDecoder.decode(name,"UTF-8"), regexMatcher.group(), session));//rootURL, String courseName, String courseNum, HttpsClient session)
                        } catch (MalformedURLException ex) {
                            print("Error creating downloadable link");
                        }
                    }
                } catch (PatternSyntaxException ex) {
                    print("Syntax error in the regular expression when trying to extract link: " + link.toString());
                }
            }
        }
        courses = mapp;
        return courses.size();
    }

    Course getCourse(String courseName){
        return courses.get(courseName);
    }


    String getCourseName(String courseInfo) {
        return courses.get(courseInfo).getCourseName();
    }
}
